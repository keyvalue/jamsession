#![allow(unused)]
extern crate failure;
extern crate fps_clock;
extern crate midir;

use failure::Error;
use std::io::{stdin, stdout, Write};
use std::thread::sleep;
use std::time::{Duration, Instant};

use midir::{Ignore, MidiInput, MidiOutput};

use midir::os::unix::VirtualOutput;

fn main() -> Result<(), Error> {
    let midi_out = MidiOutput::new("jamsession_virt_out")?;
    let mut conn_out = midi_out
        .create_virtual("outtie5k")
        .expect("cannot create device");

    // See: http://www.somascape.org/midi/tech/spec.html#sysrtmsgs
    let beat_clock_msg: Vec<u8> = vec![0xF8];
    let beat_clock_start_msg: Vec<u8> = vec![0xFA];

    // send start
    conn_out.send(&beat_clock_start_msg)?;

    // TODO: handle signals and send a stop message

    // beat clock = 24 frames per quarter note. A value of 24 FPS means 1 Quarter
    // note per second. Too slow. Double it (48) for 120 BPM, aka 2 quarter notes
    // per second.
    let mut fps = fps_clock::FpsClock::new(48);
    loop {
        // send beat clock:
        // midi message is Rust's byte slice: &[u8]
        // & means "borrowed" in rust
        // We can borrow a ref to a Vec<u8> as &[u8]
        conn_out.send(&beat_clock_msg)?;
        fps.tick();
    }
    Ok(())
}


