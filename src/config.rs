use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use toml;

pub struct MidiPorts {
    inputs: HashMap<String, String>,
    outputs: HashMap<String, String>,
}

#[derive(Deserialize)]
pub struct Config {}

impl Config {
    /// Builds a Config from the default location at $HOME/.config/jamsession/config.toml
    pub fn from_env() -> Result<Self, ConfigError> {
        let user_home = env::var("HOME").expect("HOME is unset");
        let path = Path::new(&user_home).join(".config/jamsession/config.toml");
        let mut file = File::open(&path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let conf: Config = toml::from_str(&contents)?;

        Ok(Config {})
    }
}

#[derive(Debug, Fail)]
pub enum ConfigError {
    #[fail(display = "serialization error: {}", _0)]
    SerializationError(#[cause] toml::de::Error),
    #[fail(display = "io: {}", _0)]
    Io(#[cause] io::Error),
}

impl From<io::Error> for ConfigError {
    fn from(e: io::Error) -> Self {
        ConfigError::Io(e)
    }
}

impl From<toml::de::Error> for ConfigError {
    fn from(e: toml::de::Error) -> Self {
        ConfigError::SerializationError(e)
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_new() {}

}
