#![allow(warnings)]
extern crate midir;
extern crate rimd;

use rimd::{SMFFormat, SMFError, SMF, SMFBuilder};
use rimd::Event::*;
use std::thread::sleep;
use std::env::{Args, args};
use std::time::Duration;
use std::io::{stdin, stdout, Write};
use std::error::Error;
use std::path::{PathBuf, Path};

use midir::{MidiInput, MidiOutput, Ignore, MidiInputConnection};

fn main() {
    match run() {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err.description())
    }
}

fn run() -> Result<(), Box<Error>> {
    let mut args = args();
    args.next();
    let pathstr = args.next().expect("missing arg: file");
    let pathbuf = Path::new(&pathstr);
    let f = SMF::from_file(pathbuf)?;
    

    let mut input = String::new();
    let mut output_str = String::new();
    
    let mut midi_in = MidiInput::new("My Test Input")?;
    let mut midi_out = MidiOutput::new("midi_out")?;
    
    println!("Available input ports:");
    for i in 0..midi_in.port_count() {
        println!("{}: {}", i, midi_in.port_name(i).unwrap());
    }
    print!("Please select input port: ");
    stdout().flush()?;
    stdin().read_line(&mut input)?;
    let in_port: usize = input.trim().parse()?;

    println!("Available output ports:");
    for i in 0..midi_out.port_count() {
        println!("{}: {}", i, midi_in.port_name(i).unwrap());
    }
    print!("Please select output port: ");
    stdout().flush()?;
    stdin().read_line(&mut output_str)?;
    let out_port: usize = output_str.trim().parse()?;

    let mut conn_out = midi_out.connect(out_port, "midir-test").expect("midi out fail");

    println!("\n---\nformat: {}",f.format);
    println!("tracks: {}",f.tracks.len());
    println!("division: {}",f.division);
    let mut tnum = 1;
    for track in f.tracks.iter() {
        let mut time: u64 = 0;
        println!("\n{}: {}\nevents:",tnum,track);
        tnum+=1;
        let mut midis = 0;
        let mut metas = 0;
        for event in track.events.iter() {
            match event.event {
                Midi(ref msg) => {
                    midis += 1;
                    conn_out.send(&msg.data);
                },
                Meta(ref msg) => {
                    metas += 1;
                    conn_out.send(&msg.data);
                },
            }
            //println!("  {}",event.fmt_with_time_offset(time));
            time += event.vtime;
        }
        println!("midis: {}, metas: {}", midis, metas);
    }

    
    
    // This shows how to reuse input and output objects:
    // Open/close the connections twice using the same MidiInput/MidiOutput objects
    for _ in 0..2 {
        println!("\nOpening connections");
        let log_all_bytes = Vec::new(); // We use this as an example custom data to pass into the callback
        let conn_in = midi_in.connect(in_port, "midir-test", |stamp, message, log| {
            // The last of the three callback parameters is the object that we pass in as last parameter of `connect`.
            println!("{}: {:?} (len = {})", stamp, message, message.len());
            log.extend_from_slice(message);
        }, log_all_bytes)?;
        
        // we get the connection back
        let (midi_in_, log_all_bytes) = conn_in.close();
        midi_in = midi_in_;
        println!("Connections closed");
        println!("Received bytes: {:?}", log_all_bytes);
    }
    Ok(())
}
