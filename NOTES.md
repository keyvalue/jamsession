
### 505 send clock behavior

The 505 has 3 "sync mode" types:

* INT - Use the internal clock
* SLAVE - Use a clock from a midi signal
* REMOTE - ???

On the 505, press SHIFT + 11 to cycle through various settings. 

#### Example data

This config

```
Sync Mode = INT
Sync Out = ON
```
Sends this signal over midi out

```
5921166: [248] (len = 1)
5941285: [248] (len = 1)
5962658: [248] (len = 1)
5984036: [248] (len = 1)
6005408: [248] (len = 1)
6016783: [254] (len = 1)
6025663: [248] (len = 1)
6045783: [248] (len = 1)
6067160: [248] (len = 1)
6087284: [248] (len = 1)
6108660: [248] (len = 1)
6130041: [248] (len = 1)
6150159: [248] (len = 1)
6170283: [248] (len = 1)
6191657: [248] (len = 1)
6198032: [254] (len = 1)
6213157: [248] (len = 1)
6233283: [248] (len = 1)
6254659: [248] (len = 1)
6276070: [248] (len = 1)
6296158: [248] (len = 1)
6316283: [248] (len = 1)
6337659: [248] (len = 1)
6359033: [248] (len = 1)
6379156: [254] (len = 1)
6379279: [248] (len = 1)
6400654: [248] (len = 1)
6420782: [248] (len = 1)
6442161: [248] (len = 1)
6462281: [248] (len = 1)
6483656: [248] (len = 1)
6505031: [248] (len = 1)
6525157: [248] (len = 1)
6545285: [248] (len = 1)
6560401: [254] (len = 1)
6566779: [248] (len = 1)
6588156: [248] (len = 1)
6608280: [248] (len = 1)
6629656: [248] (len = 1)
6651035: [248] (len = 1)
6671156: [248] (len = 1)
6691280: [248] (len = 1)
6712655: [248] (len = 1)
6734030: [248] (len = 1)
6741655: [254] (len = 1)
```

But setting Sync Out to OFF 

```
Sync Mode = INT
Sync Out = OFF
```

Yields this only the 254 data

```
24: [248] (len = 1)
131: [254] (len = 1)
47756: [254] (len = 1)
229129: [254] (len = 1)
410506: [254] (len = 1)
591867: [254] (len = 1)
773254: [254] (len = 1)
954624: [254] (len = 1)
1136032: [254] (len = 1)
1317372: [254] (len = 1)
1498749: [254] (len = 1)
1680126: [254] (len = 1)
1861493: [254] (len = 1)
2042870: [254] (len = 1)
2224249: [254] (len = 1)
2405623: [254] (len = 1)
2587029: [254] (len = 1)
```

Use midir's test_forward program to debug this
